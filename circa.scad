fudge_factor = 0.02;


stem_width = 2.5;
stem_length = 5;

hoop_diameter = 7;

// hoop
intersection() {
    translate([5, 0, 0]) {
        cube([10, 50, 1], center = true);
    }
    cylinder(d = hoop_diameter, center = true);
}

// stem
translate([fudge_factor, 0, 0]) {
    translate([-stem_length / 2, 0, 0]) {
        cube([5, stem_width, 1], center = true);
    }
}
