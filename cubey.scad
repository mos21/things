tallness = 2; // magic:tallness:[2,3,4]
caps = 1; // magic:caps:[1,2]
cutout = false; // magic:cutout:[true,false]

use <moslib/libchamfer.scad>;

$fn = 200; 

difference() {
    color("orange") {
        chamfered_box([25.4, 25.4, 25.4], align = [0, 0, 1], chamfer = 25.4 / 4);
    }

    translate([0, 0, -1])
    color("red") {
        cylinder(d = 4.2, h = 14);
    }
}