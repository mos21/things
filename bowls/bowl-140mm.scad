
include <../moslib/libchamfer.scad>;
include <../peglib/defaults.scad>;

wall = 5;

d =140;
r=3/5;

$fn = 150;

module bowl() {
    difference() {

        union() {
            intersection() {
                color("blue") {
                    // outer shell
                    sphere(d=d);
                }
                color("purple") {
                    // cut off the bottom of the outer shell
                    cube([d*2, d*2, (d *r)], center = true);
                }
            }
        }        

        union() {
            // remove the inside 
            intersection() {
                color("red") {
                    // inner shell
                    sphere(d=d - wall * 2);
                }
                color("purple") {
                    // cut off the bottom of the inner shell
                    cube([d*2, d*2, (d*r) - wall * 2], center = true);
                }
            }
            color("pink") {
                // the "cap" of the removal slug
                translate([0, 0, d * 0.40]) {
                    cylinder(h = (d*1/2), d = d*3, center = true);
                }
            }
        }
    }
}


intersection() {
    difference() {
        bowl();
        translate([0, 0, -((d * r)/2)]) {
            linear_extrude(height = 1) {
                rotate([180, 0, 0]) {
                    text(str(d, " mm"), size = 8, halign = "center", valign = "center");
                }
            }
        }
    }
    translate([0, 0, -100]) {
    //    cube([200,  200, 200]);
    }
}





union() {

    intersection() {
        difference() {
            color("blue") {
                // outer shell
                sphere(d=d);
            }
            color("red") {
                // inner shell
                sphere(d=d - wall * 2);
            }
        }
        color("violet"){
            translate([0, 0, d * .15]) {
                rotate_extrude() {
                    translate([d * 0.457, 0, 0]) circle(d=wall + 1);
                }
            }
        }
    }
}


