
include <../moslib/libchamfer.scad>;
include <../peglib/defaults.scad>;

wall = 5;



difference() {
    
    
    intersection() {
        // outer shell
        color("blue") {
            sphere(d=100);
        }
        color("purple") {
            cube([200, 200, 75], center = true);
        }
    }
    
    
    union() {
        // remove this part
        intersection() {
            color("red") {
                sphere(d=100 - wall * 2);
            }
            color("purple") {
                cube([200, 200, 75 - wall * 2], center = true);
            }
        }
        translate([0, 0, 25]) {
            cylinder(h = 50, d1 = 0, d2 = 300, center = true);
        }

    }



}

