use <moslib/libchamfer.scad>;

inch = 25.4;

base_width = 2 * inch;
base_height = 3 * inch;
thickness = 1/4 * inch;
chamfer = thickness / 4;

lower_leg_depth = base_height * sqrt(3) / 3;
leg_length = inch * 0.6 + thickness;

hull() 
union() {
    color("orange")
    chamfered_box(dim = [base_width, thickness, base_height], align = [0, 0, 1], chamfer = chamfer, center = false);

    color("salmon")
    chamfered_box(dim = [base_width, lower_leg_depth - thickness, thickness], align = [0, 1, 1], chamfer = chamfer, center = false);

    color("coral")
    chamfered_box(dim = [base_width, base_height - thickness, thickness], align = [0, -1, 1], chamfer = chamfer, center = false);
}
echo(thickness/4);


translate([0, lower_leg_depth - thickness -2.2 , 0]) {
    r = 30;
    color("royalblue")
    rotate([-(90-r), 0, 0]) 
    chamfered_box(dim = [base_width, thickness, leg_length], align = [0, -1, 1], chamfer = chamfer, center = false);
}
translate([0, -1 * (base_height - thickness - 1.59 ), 0]) {
    r = 45;
    color("royalblue")
    rotate([90 - r, 0, 0]) 
    chamfered_box(dim = [base_width, thickness, leg_length], align = [0, 1, 1], chamfer = chamfer, center = false);
}
