// NOSTL

scale = 0.5;
$fn = 50;

blocksize = 10 * scale;
platex = 100 * scale;
platey = 60 * scale;

mtg = 0.02;







module plate() {

    scale = 0.5;
    $fn = 50;

    blocksize = 10 * scale;
    platesize = 50 * scale;
    mtg = 0.02;
    
    
    color("blue") {
        cube([platesize + mtg, platesize + mtg, blocksize], center = true);
    }


    intersection() {
        union() {
            rotate([90, 0, 0]) {
                color("orange") hull() {
                    for(x = [-1,1]) {
                        translate([(platesize + blocksize) * x / 2, 0, 0])
                        cylinder(d = blocksize, h = platesize + 1, center = true);
                    }
                }
            }
        }

        union() {
            for(x = [-1,1]) {
                for(y = [x > 0 ? 0 : 1 : 2 : (platesize / blocksize) - 1]) {
                    translate([x * (platesize + blocksize) / 2, (-1 * platesize / 2) + (blocksize / 2) + (blocksize * y), 0]) {
                        rotate([90, 0, 0]) {
                            difference() {
                                color("steelblue") cube([blocksize,blocksize,blocksize - 0.25], center = true);
                                union() {
                                    translate([0, 0, blocksize / 2]) {
                                        color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                                    }
                                    translate([0, 0, -blocksize / 2]) {
                                        color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                                    }
                                    color("red") cylinder(d = blocksize / 2, h = blocksize, center = true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

module hinge4() {

    scale = 0.5;
    $fn = 50;

    blocksize = 10 * scale;
    platex = 60 * scale;
    platey = 50 * scale;
    
    mtg = 0.02;
    
    color("blue") {
      cube([platex, platey, blocksize], center = true);
    }



            for(x = [-1]) {
                intersection() {
    
                    union() {
                        rotate([90, 0, 0]) {
                            color("orange") hull() {
                                for(i = [0.5,1]) {
                                    for(j = [1, 0]) // drop the -1 for round hinge
                                        translate([(platex + blocksize) * i * x / 2, blocksize * j, 0])
                                            cylinder(d = blocksize, h = platey + 1, center = true);
                                    
                                }
                            }
                        }
                    }
                            

                    union() {
                        for(y = [x > 0 ? 0 : 1 : 2 : (platey / blocksize) - 1]) {
                            translate([x * (platex + blocksize) / 2, (-1 * platey / 2) + (blocksize / 2) + (blocksize * y), 0]) {
                                rotate([90, 0, 0]) {
                                    difference() {
                                        color("steelblue") cube([blocksize + mtg,blocksize,blocksize - 0.25], center = true);
                                        union() {
                                            translate([0, 0, blocksize / 2]) {
                                                color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            translate([0, 0, -blocksize / 2]) {
                                                color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            color("red") cylinder(d = blocksize / 2, h = blocksize, center = true);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }

    }
    
    
    translate([(platex-blocksize)/2, 0, -(platex-blocksize)/2]) rotate([0, -90, 0]) {
                for(x = [1]) {
                intersection() {
    
                    union() {
                        rotate([90, 0, 0]) {
                            color("orange") hull() {
                                for(i = [0.5,1]) {
                                    for(j = [-1,0])  // drop the -1 for round hinge
                                        translate([(platex + blocksize) * i * x / 2, blocksize * j, 0])
                                            cylinder(d = blocksize, h = platey + 1, center = true);

                                }
                            }
                        }
                    }
                            

                    union() {
                        for(y = [x > 0 ? 0 : 1 : 2 : (platey / blocksize) - 1]) {
                            translate([x * (platex + blocksize) / 2, (-1 * platey / 2) + (blocksize / 2) + (blocksize * y), 0]) {
                                rotate([90, 0, 0]) {
                                    difference() {
                                        color("steelblue") cube([blocksize + mtg,blocksize,blocksize/* - 0.25*/], center = true);
                                        union() {
                                            translate([0, 0, blocksize / 2]) {
                                                color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            translate([0, 0, -blocksize / 2]) {
                                                color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            color("red") cylinder(d = blocksize / 2, h = blocksize, center = true);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }
    }

}




module hinge5() {
    module comb(even = true) {
        intersection() {
            difference() {
                union() {
                    for(x = [even ? 0 : (blocksize * 3): blocksize * 6 : platex - (blocksize * 3)]) {
                        translate([x, 0, 0]) {
                            color("orange") {
                                linear_extrude(blocksize) {
                                    polygon([ [0, blocksize], [blocksize, 0], [blocksize * 2, 0], [blocksize * 3, blocksize]]);
                                }
                            }

                        }
                    }
                }
                union() {
                    color("red") {
                        translate([0, blocksize / 2, blocksize / 2]) {
                            rotate([0, 90, 0]) {
                                cylinder(d = blocksize / 2, h = platex);
                            }
                        }
                    }
                }
            }
            union() {
                color("blue") {
                    translate([0, blocksize / 2, blocksize / 2]) {
                        rotate([0, 90, 0]) { 
                            hull() {
                                cylinder(d = blocksize, h = platex);
                                translate([0, blocksize, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }           
                                /*
                                translate([blocksize, blocksize, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }
                                translate([blocksize, 0, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }
                                */

                            }
                        }
                    }
                }
            }
        }
        translate([0, blocksize, 0]) {
            cube([platex, blocksize / 2, blocksize]);
        }
    }

    comb();
    color("salmon") translate([0, blocksize, 0]) cube([platex, platey/2, blocksize]);

}




module hinge6() {

    scale = 0.5;
    $fn = 50;

    blocksize = 10 * scale;
    platex = 60 * scale;
    platey = 50 * scale;
    
    mtg = 0.02;
    


intersection(){
        difference() {
            color("steelblue") {
              cube([blocksize * 2, blocksize - 0.25, blocksize - 0.25], center = true);
            }
            for(x = [-1, 1]) {
                translate([blocksize / 2 * x, 0, 0])
                rotate([90, 0, 0]) {
                    translate([0, 0, blocksize / 2]) {
                        color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                    }
                    translate([0, 0, -blocksize / 2]) {
                        color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                    }
                    color("red") cylinder(d = blocksize / 2, h = blocksize, center = true);
                }
            }
       }


        union() {
            rotate([90, 0, 0]) {
                color("orange") hull() {
                    for(i = [-0.5,0.5]) {
                        for(j = [-1, 0])
                            translate([blocksize * i, blocksize * j, 0])
                                cylinder(d = blocksize, h = blocksize + mtg, center = true);
                        
                    }
                }
            }
        }
    }
}






// hinge3(); // flat hinge


// hinge4(); // L-shaped hinge


// hinge6(); // snail-like connector

