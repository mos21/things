
// true
// false

debug = false;



default_coaster_diameter = 150;
default_coaster_height = 10;

td = default_coaster_diameter + 25;
th = 50;
wall = 1;

$fn = 6;

difference() {
    cylinder(d = td, h = th);
    translate([0, 0, wall]) {
        cylinder(d = td - (wall * 2), h = th);
    }
}
