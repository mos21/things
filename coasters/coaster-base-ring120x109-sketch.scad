include <coaster.scad>;

base = false;
grid = false;
show_hex_grid = false;

ring_segments = 6;
ring_diameter = 110;

round_holder = true;

for(i = [0:5]) {
    echo(i);
    translate([0, 0, default_coaster_height * i]) {
        scale([1, 1, 0.5]) ring2();
    }
}
