include <coaster.scad>;

// true
// false

debug = false;

base = debug ? true : false;
grid = true;
circle_grid = true;
circle_grid_filled = true;
default_circle_outer_diameter = 15;

ring_segments = debug ? 12 : 120;
ring_diameter = 110;
default_hscale = 0.9095;

if(debug){
    color("cyan") {
        translate([-49.65, 0, 5])
        cube([0.3,200,0.5], center = true);
    }
    color("pink") {
        cube([200, 1,1], center = true);
    }
}

// EOF
