include <coaster.scad>;

base = true;
grid = false;
show_hex_grid = false;

ring_segments = 120;
ring_diameter = 110;
