// NOSTL

// true
// false

include <../moslib/libchamfer.scad>;



debug = false;


base = debug ? false: false;
grid = debug ? false: false;
holder = debug ? false: false;
round_holder = debug ? true: false;

hex_grid = debug ? false: false;
square_grid = debug ? false: false;
circle_grid = debug ? false: false;
circle_grid_filled = debug ? true : false;
diamond_grid = debug ? false: false;


//default_hscale = 0.923175;

default_hscale = 0.925;

ring_segments = 6;
ring_diameter = 150;
ring_height = 20;
//ring_wall = 5.96;
ring_wall = 6.9;

fudge_factor = 0.03;

default_hex_outer_diameter = 21;
default_square_outer_diameter = 40;
default_circle_outer_diameter = 40;
default_diamond_outer_diameter = 11;

default_hex_wall_thickness = 0.6;
minimum_hex_wall_thickness = 0.6;  // less that 0.6 seems to confused prusaslicer
default_hex_height = 10;
default_hex_alignment = [0, 0, 0];
default_extent = 5;


default_coaster_diameter = ring_diameter;
default_coaster_height = 10;

apothem_ratio = 0.433; // see: https://www.omnicalculator.com/math/hexagon


    rotational_offset = 30;



module diamond_cell(height = default_hex_height, outer_diameter = default_diamond_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    debug = false;

    assert(wall_thickness >= minimum_hex_wall_thickness);

    translate([0, 0, alignment.z * (height / 2)]) {


        difference() {
            color("blue") {
                cylinder(h = height, d = outer_diameter + fudge_factor, center = true, $fn = 6);
            }
            color("red") {
                cylinder(h = height + fudge_factor, d = outer_diameter - (wall_thickness * 2), center = true, $fn = 6);
            }
        }
    }
}



module diamond_grid(extent = 3, height = default_hex_height, outer_diameter = default_diamond_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    per_unit_horizontal_offset = outer_diameter - wall_thickness;
    per_unit_vertical_offset = (outer_diameter * cos(30)) - wall_thickness ;

    for(x = [(-(extent-1)/2) : ((extent-1)/2)]) {
        translate([per_unit_horizontal_offset * x, 0, 0]) {
            index = (extent + ((x*2) - 1)) / 2;
            x_offset = 0;
            y_offset = 0;
            translate([x_offset, y_offset, 0]) {
                for(y = [(-(extent-1)/2) : ((extent-1)/2)]) {
                    translate([0, per_unit_vertical_offset * y, 0]) {

                        hex_cell(height = height, outer_diameter = outer_diameter, wall_thickness = wall_thickness, alignment = alignment);
                    }
                }
            }
        }
    }
}






module hex_cell(height = default_hex_height, outer_diameter = default_hex_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    debug = false;

    assert(wall_thickness >= minimum_hex_wall_thickness);

    translate([0, 0, alignment.z * (height / 2)]) {
        if(debug) {
            color("orange") cube([outer_diameter, outer_diameter, 1], center = true);
            color("red") cube([5, outer_diameter * 0.433 * 2, 20], center = true);
            color("green") cube([outer_diameter, 5, 2], center = true);
        }

        difference() {
            color("blue") {
                cylinder(h = height, d = outer_diameter + fudge_factor, center = true, $fn = 6);
            }
            color("red") {
                cylinder(h = height + fudge_factor, d = outer_diameter - (wall_thickness * 2), center = true, $fn = 6);
            }
        }
    }
}



module hex_grid(extent = 3, height = default_hex_height, outer_diameter = default_hex_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    per_unit_horizontal_offset = (outer_diameter * 1.5 / 2) - wall_thickness;
    per_unit_vertical_offset = (outer_diameter - wall_thickness) * cos(30);

    for(x = [0 : extent - 1]) {
        for (xflip = [-1, 1]) {
        
        translate([xflip * per_unit_horizontal_offset * x, 0, 0]) {
            y_offset = ((x + 1) % 2 ? -(wall_thickness / 2) + (outer_diameter * cos(30) / 2): 0);
            translate([0, y_offset, 0]) {
                for(y = [0 : extent - 1]) {
                    for(yflip = [-1, 1]) {
                        translate([0, yflip * per_unit_vertical_offset * (y - 0.5), 0]) {
                            hex_cell(height = height, outer_diameter = outer_diameter, wall_thickness = wall_thickness, alignment = alignment);
                        }
                    }
                }
            }
        }
    }
    }
}




module square_cell(height = default_hex_height, outer_diameter = default_square_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    debug = false;

    assert(wall_thickness >= minimum_hex_wall_thickness);

    translate([0, 0, alignment.z * (height / 2)]) {
        if(debug) {
            color("orange") cube([outer_diameter, outer_diameter, 1], center = true);
            color("red") cube([5, outer_diameter * 0.433 * 2, 20], center = true);
            color("green") cube([outer_diameter, 5, 2], center = true);
        }

        difference() {
            color("blue") {
                cube([outer_diameter + fudge_factor, outer_diameter + fudge_factor, height], center = true);
            }
            color("red") {
                cube([outer_diameter + fudge_factor - (wall_thickness * 2), outer_diameter + fudge_factor - (wall_thickness * 2), height+ 1], center = true);
            }
        }
    }
}
module square_grid(extent = 3, height = default_hex_height, outer_diameter = default_square_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    per_unit_horizontal_offset = outer_diameter - wall_thickness;
    per_unit_vertical_offset = outer_diameter - wall_thickness;
    
    for(x = [(-(extent-1)/2) : ((extent-1)/2)]) {
        translate([per_unit_horizontal_offset * x, 0, 0]) {
            
            index = (extent + ((x*2) - 1)) / 2;
            x_offset = 0;
            y_offset = (index % 2 ? -(wall_thickness / 2) + (outer_diameter * cos(30) / 2): 0);
            
            translate([x_offset, y_offset, 0]) {
                for(y = [(-(extent-1)/2) : ((extent-1)/2)]) {
                    translate([0, per_unit_vertical_offset * y, 0]) {

                        square_cell(height = height, outer_diameter = outer_diameter, wall_thickness = wall_thickness, alignment = alignment);
                    }
                }
            }
        }
    }
}



module circle_cell(height = default_hex_height, outer_diameter = default_circle_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    debug = false;

    assert(wall_thickness >= minimum_hex_wall_thickness);

    translate([0, 0, alignment.z * (height / 2)]) {
        if(debug) {
            color("orange") cube([outer_diameter, outer_diameter, 1], center = true);
            color("red") cube([5, outer_diameter * 0.433 * 2, 20], center = true);
            color("green") cube([outer_diameter, 5, 2], center = true);
        }

        difference() {
            color("blue") {
                cylinder(h = height, d = outer_diameter + fudge_factor, center = true, $fn = 100);
            }
            color("red") {
                cylinder(h = height + fudge_factor, d = outer_diameter - (wall_thickness * 2), center = true, $fn = 100);
            }
        }
    }
}
module circle_grid(extent = 3, height = default_hex_height, outer_diameter = default_square_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    per_unit_horizontal_offset = (outer_diameter - wall_thickness) * .875  ;
    per_unit_vertical_offset = outer_diameter - wall_thickness;
    
    for(x = [(-(extent-1)/2) : ((extent-1)/2)]) {
        translate([per_unit_horizontal_offset * x, 0, 0]) {
            
            index = (extent + ((x*2) - 1)) / 2;
            x_offset = 0;
            y_offset = (index % 2 ? outer_diameter / 2: 0);
            
            translate([x_offset, y_offset, 0]) {
                for(y = [(-(extent-1)/2) : ((extent-1)/2)]) {
                    translate([0, per_unit_vertical_offset * y, 0]) {

                        circle_cell(height = height, outer_diameter = outer_diameter, wall_thickness = wall_thickness, alignment = alignment);
                    }
                }
            }
        }
    }
}

module circle_cell_filled(height = default_hex_height, outer_diameter = default_circle_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    debug = false;

    assert(wall_thickness >= minimum_hex_wall_thickness);

    translate([0, 0, alignment.z * (height / 2)]) {
        if(debug) {
            color("orange") cube([outer_diameter, outer_diameter, 1], center = true);
            color("red") cube([5, outer_diameter * 0.433 * 2, 20], center = true);
            color("green") cube([outer_diameter, 5, 2], center = true);
        }

//        difference() {
            color("blue") {
//                cylinder(h = height, d = outer_diameter + fudge_factor, center = true, $fn = 100);
            }
            color("red") {
                cylinder(h = height * 1.5, d = outer_diameter - (wall_thickness * 2), center = true, $fn = 100);
            }
//        }
    }
}
module circle_grid_filled(extent = 3, height = default_hex_height, outer_diameter = default_square_outer_diameter, wall_thickness = default_hex_wall_thickness, alignment = default_hex_alignment) {

    per_unit_horizontal_offset = (outer_diameter - wall_thickness) * .875  ;
    per_unit_vertical_offset = outer_diameter - wall_thickness;

    difference()
    {       
        color("blue") {
            cylinder(h = height, d = ring_diameter, center = true, $fn = 100);
        }

        union() {
            for(x = [(-(extent-1)/2) : ((extent-1)/2)]) {
                translate([per_unit_horizontal_offset * x, 0, 0]) {
                    
                    index = (extent + ((x*2) - 1)) / 2;
                    x_offset = 0;
                    y_offset = (index % 2 ? outer_diameter / 2: 0);
                    
                    translate([x_offset, y_offset, 0]) {
                        for(y = [(-(extent-1)/2) : ((extent-1)/2)]) {
                            translate([0, per_unit_vertical_offset * y, 0]) {

                                circle_cell_filled(height = height, outer_diameter = outer_diameter, wall_thickness = wall_thickness, alignment = alignment);
                            }
                        }
                    }
                }
            }
        }
    }

}








module ring2() {
    $fn = debug ? 12 : 100;
    hull()  {
        for(i = [rotational_offset : 360 / ring_segments : 359 + rotational_offset]) {
            rotate([0, 0, i]) {
                translate([(default_coaster_diameter - ring_height)/ 2 , 0, 0]) {
                    intersection() {
                        scale([0.95, 0.95, 1.5]) {
                            sphere(d = ring_height);
                        }
                        cylinder(d = ring_height * 0.9375, h = ring_height, center = true);
                    }
                }
            }
        }
    }
}

//difference() {
//union() {
if (base) {

    hscale = default_coaster_diameter / ring_diameter;
    vscale = default_coaster_height / ring_height;
    rotate([0, 0, debug ? 30 : -15]) {
        difference() {
           color("magenta") {
                scale([hscale, hscale, vscale]) {
                    //ring();
                    ring2();
                }
            }
            color("red") {
                rotate([0, 0, 30]) {
                    translate([0, 0, fudge_factor]) {
                        cylinder(d = ((ring_diameter - (ring_wall * 1.5)) * hscale), h = ring_height * vscale + 1, $fn = ring_segments);
                    }
                }
            }
        }
    }




}




if(grid) {
    vscale = default_coaster_height / ring_height;
    scale([1, 1, vscale]) {
        translate([0, 0, default_hex_height / 2]) {
            hscale = default_hscale;
            
            rotate([0, 0, debug ? 0 : 15]) {

                scale([hscale, hscale, 1]) {
                    difference() {
                        color("cyan") {
                            cylinder($fn = ring_segments, h = default_hex_height, d = default_coaster_diameter, center = true);
                        }
                        color("green") {
                            cylinder($fn = ring_segments, h = default_hex_height + fudge_factor, d = default_coaster_diameter - 2, center = true);
                        }
                    }
                }



                intersection() 
                {
                    scale([hscale, hscale, 1]) {
                        cylinder($fn = ring_segments, h = default_hex_height + fudge_factor * 2, d = default_coaster_diameter, center = true);
                    }
                    union() {

                        if(diamond_grid) {
                            extent = round(default_coaster_diameter / 8) + 1;
                            diamond_grid(outer_diameter = default_diamond_outer_diameter, extent = extent);
                        }
                        
                        if(hex_grid) {
                            extent = round(default_coaster_diameter / 6) + 1;
                            hex_grid(outer_diameter = default_hex_outer_diameter, extent = extent);
                        }

                        if(square_grid) {                 
                            extent = round(default_coaster_diameter / 5) + 1;
                            square_grid(outer_diameter = default_square_outer_diameter, extent = extent);
                        }
                        
                        if(circle_grid) {
                            extent = round(default_coaster_diameter / (default_circle_outer_diameter - 1)) + 1;
                            if(circle_grid_filled) {
                                circle_grid_filled(outer_diameter = default_circle_outer_diameter, extent = extent);
                            } else {
                                circle_grid(outer_diameter = default_circle_outer_diameter, extent = extent);
                            }
                            
                        }

                    }

                }
  
            }
        }
    }
}


holder_arm_width = 30;


module peg(w = holder_arm_width) {
    chamfered_box(dim = [default_coaster_height, w, default_coaster_height], align = [0, 0, 0], chamfer = default_coaster_height/4);
}


module curved_arm(angle = 60, bulge = 0) {
    $fn = 360;


    // central arc
    //
    color("orange") {
        rotate_extrude(angle = angle) {
            translate([bulge + ((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
                hull() {
                    square([default_coaster_height,     default_coaster_height / 2], center = true);
                    square([default_coaster_height / 2, default_coaster_height    ], center = true);
                }
            }
        }
    }
    
    
    // leading roundover
    //
    color("fuchsia") {
        translate([bulge + ((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
            difference() {
                chamfered_box(dim = [default_coaster_height, default_coaster_height, default_coaster_height], align = [0, 0, 0], chamfer = default_coaster_height/4);
                translate([0, default_coaster_height / 2 + 0.02, 0]) {
                    cube([default_coaster_height, default_coaster_height, default_coaster_height], center = true);
                }
            }
        }
    }

    // trailing roundover
    // 
    color("blue") {
        rotate([0, 0, angle])  {
            translate([bulge + ((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
                difference() {
                    chamfered_box(dim = [default_coaster_height, default_coaster_height, default_coaster_height], align = [0, 0, 0], chamfer = default_coaster_height/4);
                    translate([0, -default_coaster_height / 2 - 0.02, 0]) {
                        cube([default_coaster_height, default_coaster_height, default_coaster_height], center = true);
                    }
                }
            }
        }
    }
}




module holder() {
    
    // arms
    //
    for(i = [rotational_offset : 360 / ring_segments : 359 + rotational_offset]) {
        rotate([0, 0, i]) {
            translate([((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
                color("cyan") {
                    hull() {
                        peg();
                        translate([0, 0, default_coaster_height * 6]) {
                            peg();
                        }
                    }
               }
            }
        }
    }
    
    
    
    // base
    //
    color("orange") {
        
        hull() {

            for(i = [rotational_offset : 360 / ring_segments : 359 + rotational_offset]) {
                rotate([0, 0, i]) {
                    translate([((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
                        color("cyan") {
                            peg();
                        }
                    }
                }
            }
        }
    }

}




module round_holder() {
    ring_segments = 6;
    ring_diameter = 110;
    bulge = 6;

    // arms
    //    
    intersection() {
        color("red") { 
            difference() {
            cylinder(r = bulge +((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3 + default_coaster_height / 2, h = 300, $fn = 360, center = true);
            cylinder(r = bulge + ((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3 - default_coaster_height / 2, h = 300, $fn = 360, center = true);
            }
        }
        union() {
            for(i = [rotational_offset : 360 / ring_segments : 359 + rotational_offset]) {
                rotate([0, 0, i]) {

                    for(z = [0 : 0.5 : 6]) {
                        translate([0, 0, default_coaster_height * z]) {
                            curved_arm((360 / ring_segments) / 2, bulge);
                        }
                    }

                }
            }
        }
    }
    
    
    // base
    //
    color("pink") {
            $fn = 30;

        hull() {
            rotate_extrude(angle = 360, $fn = 360) {
                translate([bulge + ((default_coaster_diameter + default_coaster_height) / 2) * cos(30) + 3, 0, 0]) {
                    hull() {
                        square([default_coaster_height,     default_coaster_height / 2], center = true);
                        square([default_coaster_height / 2, default_coaster_height    ], center = true);
                    }
                }
            }
        }
    }
}    







if(holder) {
    translate([0, 0, -default_coaster_height]) {
        holder();
    }
}


if(round_holder) {
    translate([0, 0, -1 * (default_coaster_height * 1) - 0.1]) {
        round_holder();
    }
    
}


