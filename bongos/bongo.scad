// NOSTL


bongo_length = 60; // [40, 60, 80]

bongo_endcap_diameter = bongo_length / 2;

module bongo(h = bongo_length, w = bongo_endcap_diameter, scale = 1.00) {

    module knob() {
        scale([1,1,scale])  {
            difference() {
                hull() {
                    cube([w, w/2, w/2], center = true);
                    cube([w/2, w, w/2], center = true);
                    translate([0, 0, w/4]) {
                    //    cube([w/2, w/2, w/2], center = true);
                    }
                    translate([0, 0, -w * .5]) hull() {
                        cube([w/4, w/2, 0.01], center = true);
                        cube([w/2, w/4, 0.01], center = true);
                    }
                }

                color("red") {
                    rotate([0,0,15])
                    hull() {
                        translate([0, 0, 0]) {
                            cylinder(d = 0.01, h = 0.01, $fn = 8, center = true);
                        }
                        translate([0, 0, w/3]) {
                            cylinder(d = w, h = 0.01, $fn = 8, center = true);
                        }
                    }
                }
            }
        }

    }


    difference() {
        union() {
            rotate([0,90,0]) {

                color("orange") translate([0, 0, -h/2]) rotate([180, 0, 0]) knob();
                color("salmon") translate([0, 0, h/2]) knob();

                color("fuchsia") union() {
                    hull() {
                        cube([w/4, w/2, h], center = true);
                        cube([w/2, w/4, h], center = true);
                    }
                }
            }
        }

        // label
        union() {
            translate([0, 0, (w / 4) - 0.5]) {
                linear_extrude(4) {
                    text(str(w, "x",h), halign = "center", valign = "center", size = w/5);
                }
            }
        }
    }
}



// bongo();


