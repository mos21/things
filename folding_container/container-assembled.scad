// NOSTL
include <container.scad>;

color("red") hinge4b();
color("blue") translate([-(width / 2), 0, (width - blocksize * 3) / 2]) rotate([90, 0, 90]) plate();
//color("blue") translate([(width / 2) - blocksize, 0, (width + blocksize) / 2 ]) rotate([-90, 0, 90]) plate();
color("orange") translate([-blocksize, 0, width - blocksize]) rotate([0, 180, 0]) hinge4c();

color("pink") translate([0, 0, (width - blocksize) / 2]) cylinder(d = 10, h = width + mtg, center = true);
color("cyan") translate([-blocksize / 2, 0, (width - blocksize) / 2]) rotate([0, 90, 0]) cylinder(d = 10, h = width + mtg, center = true);
color("fuchsia") translate([-blocksize / 2, 0, (width - blocksize) / 2]) rotate([0, 90, 90]) cylinder(d = 10, h = length + mtg, center = true);

color("grey") translate([-blocksize/2, -(length - blocksize) / 2, (width) / 2]) rotate([90, 180, 0]) door();
color("grey") translate([-blocksize/2,  (length - blocksize) / 2, (width - blocksize*2) / 2]) rotate([90,   0, 180]) door();


for(i = [-((width / 2) - blocksize * 2) : blocksize * 2 : width / 2 - (blocksize * 2) ]) {
    translate([i, -(length - blocksize) / 2, blocksize]) {
        rotate([0, 90, 0]) {
            door_hinge();
        }
    }
}