// NOSTL

$fn = 50;

length = 125;
width = 55;

blocksize = 5;
platex = length;
platey = width;

mtg = 0.02;

module pin(len = platex, faces = 0) {

    color("mediumvioletred") {
        rotate([90, 360 / 16, 0]) {
            cylinder(d = blocksize / 2 - .1, h = len, center = true, $fn = faces ? faces : $fn);
        }
    }
    
}



module anchor(square_northwest = false, square_northeast = false, square_southwest = false, square_southeast = false, side_margin = 0.25, interior_margin = 0.40, foot_margin = 0, faces = 0) {
    
    module square_anchor() {
        difference() {
            
            // the cube
            color("steelblue") {
                translate([0, -foot_margin / 2, 0]) {
                    cube([blocksize,blocksize + foot_margin, blocksize - side_margin], center = true);
                }
            }
            
            // the hole
            color("red")  {
                    translate([0, 0, -blocksize / 2]) {
                        cylinder(r1 = blocksize / 2, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                    }
                    translate([0, 0,  blocksize / 2]) {
                        cylinder(r1 = blocksize / 6, r2 = blocksize / 2, h = (blocksize / 2), center = true);
                    }
                    cylinder(d = (blocksize / 2) + interior_margin, h = blocksize, center = true, $fn = faces ? faces : $fn);
            }
        }
    }
    
    module roundcorner() {
        
        module post() {
            cylinder(d = blocksize, h = blocksize + mtg, center = true);
        }
        module block() {
            cube([blocksize + mtg, blocksize + mtg, blocksize + mtg], center = true);
        }
        
        union() {
            color("orange") {
                
                // central post - round all corners
                post();
                
                if(square_northwest) translate([-blocksize/2,  blocksize/2, 0]) block();
                if(square_northeast) translate([ blocksize/2,  blocksize/2, 0]) block();
                if(square_southwest) translate([-blocksize/2, -blocksize/2, 0]) block();
                if(square_southeast) translate([ blocksize/2, -blocksize/2, 0]) block();

            }
        }
    }

    intersection() {
        square_anchor();    
        roundcorner();
    }


}




module plate() {

    color("blue") {
        cube([platex, platey - blocksize * 4, blocksize], center = true);
    }
    
    // odd anchors
        translate([0, (platey - blocksize * 3) / 2, 0]) {
            for(x = [blocksize : blocksize * 2 : platex - blocksize]) {
                translate([x - (platex - blocksize)/ 2, 0, 0]) {
                    rotate([0 , 90, 0]) {
                        anchor(square_southeast = true, square_southwest = true, square_northeast = true, foot_margin = 1);
                    }
                }
            }
        }
        
        
        
        // even anchors
        translate([0, -(platey - blocksize * 3) / 2, 0]) {
            for(x = [blocksize : blocksize * 2 : platex]) {
                translate([x - (platex - blocksize)/ 2 - blocksize, 0, 0]) {
                    rotate([180 , 90, 0]) {
                        anchor(square_southeast = true, square_southwest = true, square_northeast = true, foot_margin = 1);
                    }
                }
            }
        }
        
        
}

//plate();

module hinge4() {

    platex = width - blocksize;
    platey = length;
    
    
    color("blue") {
      cube([platex, platey, blocksize], center = true);
    }


            // long/horizontal leg
            for(x = [-1]) {
                intersection() {
    
                    union() {
                        rotate([90, 0, 0]) {
                            color("orange") hull() {
                                for(i = [0.5,1]) {
                                    for(j = [-1, 0]) // drop the -1 for round hinge
                                        translate([(platex + blocksize) * i * x / 2, blocksize * j, 0])
                                            cylinder(d = blocksize, h = platey + 1, center = true);
                                    
                                }
                            }
                        }
                    }
                            

                    union() {
                        for(y = [x > 0 ? 0 : 1 : 2 : (platey / blocksize) - 1]) {
                            translate([x * (platex + blocksize) / 2, (-1 * platey / 2) + (blocksize / 2) + (blocksize * y), 0]) {
                                rotate([90, 0, 0]) {
                                    difference() {
                                        color("steelblue") cube([blocksize + mtg,blocksize,blocksize - 0.25], center = true);
                                        union() {
                                            translate([0, 0, blocksize / 2]) {
                                                color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            translate([0, 0, -blocksize / 2]) {
                                                color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            color("red") cylinder(d = blocksize / 2 + 0.4, h = blocksize, center = true);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }

    }
    
    
    // short/vertical leg    
    translate([(platex-blocksize)/2, 0, -((platex-blocksize)/2 - blocksize)]) rotate([0, -90, 0]) {
                for(x = [1]) {
                intersection() {
    
                    union() {
                        rotate([90, 0, 0]) {
                            color("orange") hull() {
                                for(i = [0.5,1]) {
                                    for(j = [-1,0])  // drop the -1 for round hinge
                                        translate([(platex + blocksize) * i * x / 2, blocksize * j, 0])
                                            cylinder(d = blocksize, h = platey + 1, center = true);

                                }
                            }
                        }
                    }
                            

                    union() {
                        for(y = [x > 0 ? 0 : 1 : 2 : (platey / blocksize) - 1]) {
                            translate([x * (platex + blocksize) / 2, (-1 * platey / 2) + (blocksize / 2) + (blocksize * y), 0]) {
                                rotate([90, 0, 0]) {
                                    difference() {
                                        translate([-blocksize / 2, 0, 0])
                                        color("steelblue") cube([blocksize * 2 + mtg,blocksize,blocksize/* - 0.25*/], center = true);
                                        union() {
                                            translate([0, 0, blocksize / 2]) {
                                                color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            translate([0, 0, -blocksize / 2]) {
                                                color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                                            }
                                            color("red") cylinder(d = blocksize / 2 + 0.4, h = blocksize, center = true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    

            }
            
        }
    }

    translate([(platex - blocksize) / 2, 0, blocksize]) color("steelblue") cube([blocksize, platey, blocksize + mtg], center = true);



}



module hinge5() {
    module comb(even = true) {
        intersection() {
            difference() {
                union() {
                    for(x = [even ? 0 : (blocksize * 3): blocksize * 6 : platex - (blocksize * 3)]) {
                        translate([x, 0, 0]) {
                            color("orange") {
                                linear_extrude(blocksize) {
                                    polygon([ [0, blocksize], [blocksize, 0], [blocksize * 2, 0], [blocksize * 3, blocksize]]);
                                }
                            }

                        }
                    }
                }
                union() {
                    color("red") {
                        translate([0, blocksize / 2, blocksize / 2]) {
                            rotate([0, 90, 0]) {
                                cylinder(d = blocksize / 2, h = platex);
                            }
                        }
                    }
                }
            }
            union() {
                color("blue") {
                    translate([0, blocksize / 2, blocksize / 2]) {
                        rotate([0, 90, 0]) { 
                            hull() {
                                cylinder(d = blocksize, h = platex);
                                translate([0, blocksize, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }           
                                /*
                                translate([blocksize, blocksize, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }
                                translate([blocksize, 0, 0]) {
                                    cylinder(d = blocksize, h = platex);
                                }
                                */

                            }
                        }
                    }
                }
            }
        }
        translate([0, blocksize, 0]) {
            cube([platex, blocksize / 2, blocksize]);
        }
    }

    comb();
    color("salmon") translate([0, blocksize, 0]) cube([platex, platey/2, blocksize]);

}




module hinge6() {

    scale = 0.5;
    $fn = 50;

    blocksize = 10 * scale;
    platex = 60 * scale;
    platey = 50 * scale;
    
    mtg = 0.02;
    


intersection(){
        difference() {
            color("steelblue") {
              cube([blocksize * 2, blocksize - 0.25, blocksize - 0.25], center = true);
            }
            for(x = [-1, 1]) {
                translate([blocksize / 2 * x, 0, 0])
                rotate([90, 0, 0]) {
                    translate([0, 0, blocksize / 2]) {
                        color("red") cylinder(r2 = (blocksize / 2) - mtg, r1 = blocksize / 6, h = (blocksize / 2), center = true);
                    }
                    translate([0, 0, -blocksize / 2]) {
                        color("red") cylinder(r1 = (blocksize / 2) - mtg, r2 = blocksize / 6, h = (blocksize / 2), center = true);
                    }
                    color("red") cylinder(d = blocksize / 2, h = blocksize, center = true);
                }
            }
       }


        union() {
            rotate([90, 0, 0]) {
                color("orange") hull() {
                    for(i = [-0.5,0.5]) {
                        for(j = [-1, 0])
                            translate([blocksize * i, blocksize * j, 0])
                                cylinder(d = blocksize, h = blocksize + mtg, center = true);
                        
                    }
                }
            }
        }
    }
}






// hinge3(); // flat hinge


//hinge4(); // L-shaped hinge

module hinge4b() {
    
    difference () {
        union() {
            // carve out space for comb
            difference() {
                hinge4();
                translate([-blocksize/2, -(length - blocksize) / 2, 0]) {
                    cube([mtg + width - (blocksize * 2), mtg + blocksize, mtg + blocksize], center = true);
                }
            }
            
            // comb
        //    module anchor(square_northwest = false, square_northeast = false, square_southwest = false, square_southeast = false, side_margin = 0.25, interior_margin = 0.40, foot_margin = 0)
            translate([-(width - blocksize * 2) / 2, -(length - blocksize) / 2, 0]) {
                for(i = [0 : blocksize * 2 : width - (blocksize * 3)]) {
                    echo(i);
                    translate([i, 0, 0]) {
                        rotate([0, 90, 180]) {
                            anchor(square_southwest = true, square_southeast = true, foot_margin = 1);
                        }
                    }
                }
            }
        }
        
        union() {
            // notch out top for door to engage
            color("lightgreen") {
                translate([-blocksize / 2 -mtg/2, (length - blocksize/2)/ 2,  blocksize / 4]) {
                    cube([width - blocksize * 2 + mtg, blocksize/2 + mtg, blocksize/2 + mtg], center = true);
                }
            }
        }
    }
    
}


module hinge4c() {


    difference() {    
        union() {
            
            // carve out space for comb
            difference() {
                hinge4();
                translate([-blocksize/2, (length - blocksize) / 2, 0]) {
                    cube([mtg + width - (blocksize * 2), mtg + blocksize, mtg + blocksize], center = true);
                }
            }
            
            // comb
        //    module anchor(square_northwest = false, square_northeast = false, square_southwest = false, square_southeast = false, side_margin = 0.25, interior_margin = 0.40, foot_margin = 0)
            translate([-(width - blocksize * 2) / 2, (length - blocksize) / 2, 0]) {
                for(i = [0 : blocksize * 2 : width - (blocksize * 3)]) {
                    echo(i);
                    translate([i, 0, 0]) {
                        rotate([0, 90, 0]) {
                            anchor(square_southwest = true, square_southeast = true, foot_margin = 1);
                        }
                    }
                }
            }
        }    
        
        union() {
            // notch out top for door to engage
            color("lightgreen") {
                translate([-blocksize / 2 -mtg/2, - (length - blocksize/2)/ 2,  blocksize / 4]) {
                    cube([width - blocksize * 2 + mtg, blocksize/2 + mtg, blocksize/2 + mtg], center = true);
                }
            }
        }
    }

    
}


module door() {
    difference() {
        cube([width - blocksize * 2, width - blocksize * 3, blocksize], center = true);
        translate([0, 0, blocksize/2]) {
            color("red") cube([blocksize*2, blocksize*2, blocksize * 1.5], center = true);
        }
    }
    color("lightgreen") {
        translate([0, -(width - blocksize * 2.5) /2, blocksize/4]) {
            cube([width - blocksize * 2, blocksize/2 + mtg, blocksize/2], center = true);
        }
    }
    
    translate([0, 0, (blocksize / 4) + (blocksize / 8)])
    color("orange") cube([blocksize * 3, blocksize / 2, blocksize / 4], center = true);

    
    // comb
    //    module anchor(square_northwest = false, square_northeast = false, square_southwest = false, square_southeast = false, side_margin = 0.25, interior_margin = 0.40, foot_margin = 0)
    translate([-((width - blocksize * 3) / 2), (width - blocksize * 2) / 2, 0]) {
        for(i = [0 : blocksize * 2 : width - (blocksize * 3)]) {
            translate([i, 0, 0]) {
                rotate([0, 90, 0]) {
                    anchor(square_southwest = true, square_southeast = true, foot_margin = 1);
                }
            }
        }
    }


}


module door_hinge(faces = 0) {
    rotate([0, 0, 0]) {
        anchor( square_southeast = true, square_northeast = true, square_southwest = true, faces = faces);
        translate([blocksize, 0, 0]) {
            anchor(square_southwest = true, square_northwest = true, faces = faces);
        }
    }
}

//door();


//hinge4c();



// hinge6(); // snail-like connector

