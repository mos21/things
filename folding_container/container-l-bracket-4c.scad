include <container.scad>;

difference() {
    hinge4c(); // L-shaped hinge
    translate([0, 0, blocksize/2]) rotate([0, 0, 90]) text("C", font = "monospace", size = 20, halign = "center", valign = "center");
}


//color("red") translate([-2.5, 0, 0]) rotate([0, 90, 0]) cylinder(d = 10, h = 50, center = true);