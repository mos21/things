include <container.scad>;

rotate([0, 0, 0]) {
    anchor(square_southwest = true, square_southeast = true, square_northeast = true);
    translate([blocksize, 0, 0]) {
        anchor(square_southwest = true, square_southeast = true, square_northwest = true);
    }
}
