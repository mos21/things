include <container.scad>;

difference() {
    hinge4b(); // L-shaped hinge
    translate([0, 0, blocksize/2]) rotate([0, 0, 90]) text("B", size = 20, halign = "center", valign = "center");
}

//color("red") translate([-2.5, 0, 0]) rotate([0, 90, 0]) cylinder(d = 10, h = 50, center = true);

//translate([-width/2, 0, 0]) pin(length);

