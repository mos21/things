// true
// false

alignment = "true";

blocksize = 5;

module comb(extent, alignment) {
    
    assert(alignment == "even" || alignment == "odd");

    starting_offset = alignment == "even" ? 1 : 2;
    
translate([-(blocksize * (extent + 1)) / 2, 0, 0]) {

    for(i = [starting_offset : 2 : extent]) {

        echo(i);

        translate([blocksize * i, 0, 0]) {
            if(alignment == "even") {
                color("blue") cube([blocksize, blocksize, blocksize], center = true);
            } else {
                color("fuchsia") cube([blocksize, blocksize, blocksize], center = true);
            }
        }        
    }
}    
}

z=21;
comb(z, "even");
translate([0,0,1]) comb(z, "odd");