



foo = 4; 
bar = 40;


minkowski() {

    hull() {
        cube(bar - foo, center = true);
        translate([30, 0, 0]) cube(10, center = true);
    }
    intersection() {
        rotate([45, 0, 0])
        cube(foo, center = true);

        rotate([0, 0, 45])
        cube(foo, center = true);

        rotate([0, 45, 0])
        cube(foo, center = true);
    }
}


intersection() {
    rotate([45, 0, 0]) color("pink") cube(bar, center = true);
    rotate([0, 0, 45]) color("pink") cube(bar, center = true);
    rotate([0, 45, 0]) color("pink") cube(bar, center = true);
}