include <moslib/libchamfer.scad>;


$fn = 50; 


    color("orange") {
        chamfered_box([12.5, 12.5, 5], align = [0, 0, 4], chamfer = 1);
        chamfered_box([12.5, 12.5, 5], align = [0, 0, -4], chamfer = 1);
    }
    
    color("blue") {
        hull() {
            chamfered_box([12.5, 5, 20], chamfer = 1);
            chamfered_box([ 5, 12.5, 20], chamfer = 1);
        }
    }


color("green") {

    translate([0, 0, 10]) {
        for(align = [-1, 3]) {

            hull() {
                chamfered_box([12.5, 2.5, 7.5], align = [0, align, 1], chamfer = 2.5/4);
                chamfered_box([5, 2.5, 11.25], align = [0, align, 1], chamfer = 2.5/4);
            }
        }
        
    }
    
    
    
  
    translate([0, 0, -10]) {
        for(align = [1, -3]) {

            hull() {
                chamfered_box([12.5, 2.5, 7.5], align = [0, align, -1], chamfer = 2.5/4);
                chamfered_box([5, 2.5, 11.25], align = [0, align, -1], chamfer = 2.5/4);
            }
        }
    }        

        /* chamfered_box([10, 2.5, 12.5], align = [0, -3, -1], chamfer = 2.5/4);
    translate([0, 0, -10]) chamfered_box([10, 2.5, 12.5], align = [0, 1, -1], chamfer = 2.5/4);
  */  
    
}


rotate([90, 0, 0]) {
    difference() {
        color("red") cylinder(h = 20, d = 10, center = true, $fn = 6);
        color("yellow") cylinder(h=21, d=8, center = true, $fn = 6);
    }
    color("orange") cylinder(h = 19, d=8, center = true, $fn = 6);
}
