
// $fn = 250;
$fn = 25;

edge = 5;

foo = 50;
bar = 30;

module edger() {
    intersection() {
        rotate([45, 0, 0]) color("pink") cube(edge, center = true);
        rotate([0, 0, 45]) color("pink") cube(edge, center = true);
        rotate([0, 45, 0]) color("pink") cube(edge, center = true);
    }
}


rotate([180, 0, 0]) {

    minkowski() {
        difference() {
            difference() {
                cylinder(d = foo, h = foo, center = true);
                cylinder(d = bar, h = foo + 2, center = true);
            }

            color("red") {
                translate([foo/2, 0, -foo]) {
                    rotate([0, 60, 0]) {
                        cube(foo * 2, center = true);
                    }
                }
            }
        }


        edger();

    }

}
