use <moslib/libchamfer.scad>;

inch = 25.4;

thickness = 1/4 * inch;
chamfer = thickness / 8;


base_width = 2 * inch;
phone_width = 3.25 * inch;
phone_length = 5.5 * inch;
phone_depth = 5/8 * inch;



module leg(len) {
    difference(){
    color("royalblue") {
//        chamfered_box(dim = [base_width / 4, thickness, phone_depth + thickness], align = [ 3, 1, 1], chamfer = chamfer, center = false);
//        chamfered_box(dim = [base_width / 4, thickness, phone_depth + thickness], align = [-3, 1, 1], chamfer = chamfer, center = false);
        
        chamfered_box(dim = [base_width, thickness, phone_depth + thickness], align = [0, 1, 1], chamfer = chamfer, center = false);

    }
color("red") {
        chamfered_box(dim = [base_width / 2, thickness * 2, 2 * (phone_depth + thickness)], align = [0, 1.5, 0.5], chamfer = chamfer * 4, center = false);


    }
}
    color("cyan") {
        chamfered_box(dim = [base_width, thickness/2, phone_depth + thickness], align = [0, 1, 1], chamfer = chamfer, center = false);
    }
    color("lightblue") {
        chamfered_box(dim = [base_width, len + thickness, thickness], align = [0, 1, 1], chamfer = chamfer, center = false);

    }
}


leg(phone_length);

translate([0, phone_length, -phone_width]) 
rotate([90, 0, 180])
leg(phone_width);