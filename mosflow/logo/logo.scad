$fn = 36;

big_ball_size = 10;
ball_spacing = 30;
little_ball_size = 5;


module big_balls() {
    color("tomato") union() {
        sphere(big_ball_size);


        translate([ball_spacing, 0, 0]) {
            sphere(big_ball_size);
        }

        translate([0, 0, -ball_spacing]) {
            sphere(big_ball_size);
        }
    }
}


module little_balls() {
    color("lightblue") union() {
        sphere(5);

        translate([30, 0, 0]) {
            sphere(5);
        }

        translate([0, 0, -30]) {
            sphere(5);
        }
    }
}


module cross () {
    color("tomato")  union() {
        cylinder(h = ball_spacing, d = big_ball_size, center = true);
        rotate([90, 0, 0]) {
            cylinder(h = ball_spacing, d = big_ball_size, center = true);
        }
        rotate([0, 90, 0]) {
            cylinder(h = ball_spacing, d = big_ball_size, center = true);
        }
    }
}


difference() {
    big_balls();
    cross();
    translate([ball_spacing, 0, 0]) {
        cross();
    }
    translate([0, 0, -ball_spacing]) {
        cross();
    }
}
    


little_balls();


color("lightblue") {
    rotate([0, 90, 0]) {
        cylinder(h = 30, d = 5);
    }

    rotate([0, 180, 0]) { 
        cylinder(h = 30, d = 5);
    }
}